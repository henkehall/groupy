﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupyLogic
{
    public class RelationHandler
    {
        public int Id { get; set; }
        PersonRelation[][] relationMatrix;
        int numberOfPeople;
        Dictionary<int, int> indexToId = new Dictionary<int, int>();
        Random rnd;
        [Required]
        public Setting Setting { get; set; }

        public PersonRelation[][] RelationMatrix
        {
            get
            {
                return relationMatrix;
            }
            set
            {
                relationMatrix = value;
            }
        }

        public RelationHandler(Person[] persons)
        {
            this.numberOfPeople = persons.Length;
            relationMatrix = new PersonRelation[numberOfPeople][];
            for (int i = 0; i < numberOfPeople; i++)
            {
                indexToId.Add(i, persons[i].Id);
                relationMatrix[i] = new PersonRelation[i];
                for (int j = 0; j < i; j++)
                {
                    relationMatrix[i][j] = new PersonRelation();
                }
            }
            rnd = new Random();
        }

        //cw for debugging purposes
        public void PrintRelationMatrix_TimesWorked()
        {
            for (int i = 0; i < relationMatrix.Count(); i++)
            {
                Console.Write("P " + i.ToString("D2"));
                for (int j = 0; j < relationMatrix[i].Count(); j++)
                {
                    switch (relationMatrix[i][j].TimesWorked)
                    {

                        case 1:
                            Console.ForegroundColor = ConsoleColor.Green;
                            break;
                        case 2:
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            break;
                        case 3:
                            Console.ForegroundColor = ConsoleColor.Blue;
                            break;
                        case 4:
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            break;
                        case 5:
                            Console.ForegroundColor = ConsoleColor.Magenta;
                            break;
                        case 6:
                            Console.ForegroundColor = ConsoleColor.Red;
                            break;
                        default:
                            Console.ForegroundColor = ConsoleColor.White;
                            break;
                    }
                    Console.Write("   " + relationMatrix[i][j].TimesWorked);

                }
                Console.ResetColor();
                Console.WriteLine();
            }
            Console.Write("    ");
            for (int i = 0; i < relationMatrix.Count(); i++)
            {
                Console.Write($"  {i.ToString("D2")}");
            }
            Console.WriteLine();
        }

        //not used
        public List<Tuple<int, int, int>> OrderedRelationsList
        {
            get
            {
                var rnd = new Random();
                List<Tuple<int, int, int>> relationsList = new List<Tuple<int, int, int>>();
                for (int i = 0; i < relationMatrix.Length; i++)
                {
                    for (int j = 0; j < i; j++)
                    {
                        relationsList.Add(Tuple.Create(i, j, relationMatrix[i][j].TimesWorked));
                    }
                }
                
                relationsList = relationsList.OrderBy(r => r.Item3).ThenByDescending(
                    t => Math.Max(MaxTimesWorkedTogether(t.Item1), MaxTimesWorkedTogether(t.Item2)))
                    .ThenBy(r => rnd.Next()).ToList();
                //To do: then order by maximum worked with (have to add that value to tuple?)

                foreach (var item in relationsList)
                {
                    Console.WriteLine(item.Item1 + " " + item.Item2 + " " + item.Item3);
                }
                return relationsList;
            }
        }

        // 
        public List<Tuple<int, int>> OrderedTimesWorkedList
        {
            get
            {
                var timesWorkedList = new List<Tuple<int, int>>();
                for (int i = 0; i < relationMatrix.Length; i++)
                {
                    timesWorkedList.Add(Tuple.Create(i, MaxTimesWorkedTogether(i)));
                }
                timesWorkedList = timesWorkedList.OrderByDescending(t => t.Item2).ThenBy(t => rnd.Next())
                    .ToList();
                //Console.WriteLine("===========");
                //foreach (var item in timesWorkedList)
                //{
                //    Console.WriteLine(item.Item1 + " " + item.Item2);
                //}
                return timesWorkedList;
            }
        }

        public void NewGroupRelation(int p1, int p2)
        {
            if (p1 != p2)
            {
                if (p1 > p2)
                {
                    relationMatrix[p1][p2].AddTimesWorked();
                }
                else
                {
                    relationMatrix[p2][p1].AddTimesWorked();
                }
            }
        }

        public int TimesWorkedTogether(int p1, int p2)
        {
            if (p1 != p2)
            {
                if (p1 > p2)
                {
                    return relationMatrix[p1][p2].TimesWorked;
                }
                else
                {
                    return relationMatrix[p2][p1].TimesWorked;
                }
            }
            return 0;
        }

        public int MaxTimesWorkedTogether(int p1)
        {
            int maxTimes = int.MinValue;
            for (int i = 0; i < numberOfPeople; i++)
            {
                if (p1!=i)
                {
                    if (p1 > i)
                    {
                        if (relationMatrix[p1][i].TimesWorked>maxTimes)
                        {
                            maxTimes = relationMatrix[p1][i].TimesWorked;
                        }
                    }
                    else
                    {
                        if (relationMatrix[i][p1].TimesWorked > maxTimes)
                        {
                            maxTimes = relationMatrix[i][p1].TimesWorked;
                        }
                    }
                }

            }
            return maxTimes;
        }
        public int MaxTimesWorkedTogether(int p1, int[] groupmembers)
        {
            int maxTimes = int.MinValue;
            for (int i = 0; i < groupmembers.Length; i++)
            {
                if (p1 != groupmembers[i])
                {
                    if (p1 > groupmembers[i])
                    {
                        maxTimes = Math.Max(maxTimes, relationMatrix[p1][groupmembers[i]].TimesWorked);
                    }
                    else
                    {
                        maxTimes = Math.Max(maxTimes, relationMatrix[groupmembers[i]][p1].TimesWorked);
                    }
                }
            }
            return maxTimes;
        }

        public int IndexToId(int index)
        {
            if (indexToId.ContainsKey(index))
            {
                return indexToId[index];
            }
            else
            {
                Console.WriteLine("ERROR, no such key!");
                return -1;
            }
        }
    }
}
