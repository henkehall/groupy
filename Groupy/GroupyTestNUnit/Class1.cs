﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupyTestNUnit
{
    [TestFixture]
    public class GroupyTetst
    {
        [Test]
        public void ShouldFindStardardDeviation()
        {
            var array = new int[] { 5, 5, 5, 5, 5, 5, 5 };
            double result = GroupyLogic.Mathematics.StandardDeviation(array);

            Assert.That(result, Is.EqualTo(0));
        }
        [Test]
        public void ShouldFindStardardDeviation2()
        {
            var array2 = new int[] { 1,5,2,10,20,0 };
            double result2 = GroupyLogic.Mathematics.StandardDeviation(array2);
            Assert.That(result2, Is.EqualTo(7.607).Within(0.01));
        }
    }
}
