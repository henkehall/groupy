﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupyLogic
{
    public static class Mathematics
    {


        // copied from
        // https://stackoverflow.com/a/26662496
        //
        public static double StandardDeviation(int[] data)
        {
            double stdDev = 0;
            double sumAll = 0;
            double sumAllQ = 0;

            //Sum of x and sum of x²
            for (int i = 0; i < data.Length; i++)
            {
                int x = data[i];
                sumAll += x;
                sumAllQ += x * x;
            }

            //Mean (not used here)
            //double mean = 0;
            //mean = sumAll / (double)data.Length;

            //Standard deviation
            stdDev = System.Math.Sqrt(
                (sumAllQ -
                (sumAll * sumAll) / data.Length) *
                (1.0d / (data.Length - 1))
                );

            return stdDev;
        }

        // copied from
        // https://stackoverflow.com/a/38114328
        //
        public static int Factorial(int n)
        {
            if (n >= 2) return n * Factorial(n - 1);
            return 1;
        }

        public static int Median(int[] xs)
        {
            Array.Sort(xs);
            return xs[xs.Length / 2];
        }
    }
}
