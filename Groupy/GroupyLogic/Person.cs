﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupyLogic
{
    public class Person
    {
        static int incrimentalID = 0;
        private string name;
        private int id;

        public Person(string name)
        {
            this.name = name;
            this.id = incrimentalID;
            incrimentalID++;
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
    }
}
