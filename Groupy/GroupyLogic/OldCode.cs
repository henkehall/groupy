﻿private void PopulateGroupFetchBruteforce(Person[] persons, RelationHandler relations)
{
    List<Group>[] groupSetAlternatives = new List<Group>[100];
    double[] groupSetAltPointsStdDev = new double[groupSetAlternatives.Length];
    int[][] groupSetAltPoints = new int[groupSetAlternatives.Length][];
    int numberOfAlternatives = -1;

    for (int altNbr = 0; altNbr < groupSetAlternatives.Length; altNbr++)
    {
        //create new groups
        groupSetAlternatives[altNbr] = new List<Group>();
        for (int i = 0; i < groups.Count; i++)
        {
            groupSetAlternatives[altNbr].Add(new Group());
        }
        List<Tuple<int, int>> peopleList = relations.OrderedTimesWorkedList;

        int groupMaxSize = persons.Length / groupSetAlternatives[altNbr].Count;
        int slotsForGroupsWithMaxSize = groupSetAlternatives[altNbr].Count;
        if (persons.Length % groupSetAlternatives[altNbr].Count > 0)
        {
            groupMaxSize += 1;
            slotsForGroupsWithMaxSize = persons.Length % groupSetAlternatives[altNbr].Count;
        }


        Random rnd = new Random();
        int randomPerson = rnd.Next(0, persons.Length - 1);

        //add first person to group one to get started
        groupSetAlternatives[altNbr][0].AddMember(persons.SingleOrDefault(item => item.Id == peopleList[randomPerson].Item1));

        //remove the person that was just added to groups
        peopleList.RemoveAt(randomPerson);

        while (peopleList.Count > 0)
        {
            for (int i = 0; i < groupSetAlternatives[altNbr].Count; i++)
            {
                bool added = false;
                if ((groupSetAlternatives[altNbr][i].MembersArray.Length < groupMaxSize && slotsForGroupsWithMaxSize > 0)
                   || groupSetAlternatives[altNbr][i].MembersArray.Length < groupMaxSize - 1) //if group has at lest one space left
                {
                    //this block is to find the person that has worked the leat times with current members
                    int[] currentMembers = new int[groupSetAlternatives[altNbr][i].MembersArray.Length];
                    for (int j = 0; j < currentMembers.Length; j++)
                    {
                        currentMembers[j] = groupSetAlternatives[altNbr][i].MembersArray[j].Id;
                    }
                    int minPoints = int.MaxValue;
                    int indexOfPersonLeastPoints = 0;
                    //Console.WriteLine("zeroing minPoints");
                    for (int j = 0; j < peopleList.Count; j++)
                    {
                        int points = 0;

                        for (int k = 0; k < currentMembers.Length; k++)
                        {
                            points += relations.TimesWorkedTogether(
                                peopleList[j].Item1, currentMembers[k]);
                            //Console.WriteLine($"comparing p {peopleList[j].Item1} and " +
                            //    $"p {currentMembers[k]}. the points are {relations.TimesWorkedTogether(peopleList[j].Item1, currentMembers[k])}, totpoints: {points}");
                        }
                        if (points < minPoints)
                        {
                            minPoints = points;
                            indexOfPersonLeastPoints = j;
                            //Console.WriteLine($"new lowest for p {peopleList[j].Item1}");
                        }
                    }

                    groupSetAlternatives[altNbr][i].AddMember(persons.SingleOrDefault(
                            p => p.Id == peopleList[indexOfPersonLeastPoints].Item1));
                    peopleList.RemoveAt(indexOfPersonLeastPoints);

                    //remove one slot from groups able to have max number of members if max members in group
                    if (groupSetAlternatives[altNbr][i].NumberOfMembers == groupMaxSize)
                    {
                        slotsForGroupsWithMaxSize--;
                    }

                    added = true;
                }

                if (added)
                    break;
            }
        }
        int numberOfRelations = 0;
        //calculating the number of relations
        for (int i = 0; i < groupSetAlternatives[altNbr].Count; i++)//loop the groups
        {
            numberOfRelations += Mathematics.Factorial(groupSetAlternatives[altNbr][i].MembersArray.Count()) /
                (2 * Mathematics.Factorial((groupSetAlternatives[altNbr][i].MembersArray.Count() - 2)));
        }

        //get the standard deviation and median of the new number of times worked together for everyone in the groupset
        groupSetAltPoints[altNbr] = new int[numberOfRelations];
        int relationCount = 0;
        for (int i = 0; i < groupSetAlternatives[altNbr].Count; i++)//loop the groups
        {
            for (int j = 0; j < groupSetAlternatives[altNbr][i].MembersArray.Count(); j++)
            {
                for (int k = 0; k < j; k++)
                {
                    if (j != k)
                    {
                        groupSetAltPoints[altNbr][relationCount] = relations.TimesWorkedTogether(groupSetAlternatives[altNbr][i].MembersArray[j].Id, groupSetAlternatives[altNbr][i].MembersArray[k].Id) + 1;
                        relationCount++;
                    }
                }
            }
        }
        groupSetAltPointsStdDev[altNbr] = Mathematics.StandardDeviation(groupSetAltPoints[altNbr]);

        numberOfAlternatives++;
        if (groupSetAltPointsStdDev[altNbr] == 0)
        {
            break;
        }
    }
    double bestGroupSet = int.MaxValue;
    for (int i = 0; i < groupSetAlternatives.Length; i++)
    {
        //Console.WriteLine("standard Dev: "+groupSetAltPointsStdDev[i]);
        if (groupSetAltPointsStdDev[i] < bestGroupSet)
        {
            bestGroupSet = groupSetAltPointsStdDev[i];
            groups = groupSetAlternatives[i];
            Console.WriteLine("standard Dev: " + groupSetAltPointsStdDev[i]);
        }
    }
    //get the n different alternatives and find the best one and set it to groups
}




private void PopulateGroupFetch(Person[] persons, RelationCalc relations)
{
    //List<Tuple<int, int, int>> relationList = relations.OrderedRelationsList;
    List<Tuple<int, int>> peopleList = relations.OrderedTimesWorkedList;

    int groupMaxSize = persons.Length / groups.Count;
    int slotsForGroupsWithMaxSize = groups.Count;
    if (persons.Length % groups.Count > 0)
    {
        groupMaxSize += 1;
        slotsForGroupsWithMaxSize = persons.Length % groups.Count;
    }
    //add first person to group one to get started
    groups[0].AddMember(persons.SingleOrDefault(item => item.Id == peopleList[0].Item1));

    //remove the person that was just added to groups
    peopleList.RemoveAt(0);

    while (peopleList.Count > 0)
    {
        for (int i = 0; i < groups.Count; i++)
        {
            bool added = false;
            if ((groups[i].Members.Length < groupMaxSize && slotsForGroupsWithMaxSize > 0)
               || groups[i].Members.Length < groupMaxSize - 1) //if group has at lest one space left
            {
                //this block is to find the person that has worked the leat times with current members
                int[] currentMembers = new int[groups[i].Members.Length];
                for (int j = 0; j < currentMembers.Length; j++)
                {
                    currentMembers[j] = groups[i].Members[j].Id;
                }
                int minPoints = int.MaxValue;
                int indexOfPersonLeastPoints = 0;
                //Console.WriteLine("zeroing minPoints");
                for (int j = 0; j < peopleList.Count; j++)
                {
                    int points = 0;

                    for (int k = 0; k < currentMembers.Length; k++)
                    {
                        points += relations.TimesWorkedTogether(
                            peopleList[j].Item1, currentMembers[k]);
                        //Console.WriteLine($"comparing p {peopleList[j].Item1} and " +
                        //    $"p {currentMembers[k]}. the points are {relations.TimesWorkedTogether(peopleList[j].Item1, currentMembers[k])}, totpoints: {points}");
                    }
                    if (points < minPoints)
                    {
                        minPoints = points;
                        indexOfPersonLeastPoints = j;
                        //Console.WriteLine($"new lowest for p {peopleList[j].Item1}");
                    }
                }

                groups[i].AddMember(persons.SingleOrDefault(
                        p => p.Id == peopleList[indexOfPersonLeastPoints].Item1));
                peopleList.RemoveAt(indexOfPersonLeastPoints);

                //remove one slot from groups able to have max number of members if max members in group
                if (groups[i].NumberOfMembers == groupMaxSize)
                {
                    slotsForGroupsWithMaxSize--;
                }

                added = true;
            }

            if (added)
                break;
        }
    }
}

// potential  of bruteforcing fix
// make a few passes, 
// order groupsets by new potential worked together maximum, then by count of worked tohether maximum

private void PopulateGroups3(Person[] persons, RelationCalc relations) // mod of first
{
    List<Tuple<int, int, int>> relationList = relations.OrderedRelationsList;

    int groupMaxSize = persons.Length / groups.Count;
    int slotsForGroupsWithMaxSize = groups.Count;
    if (persons.Length % groups.Count > 0)
    {
        groupMaxSize += 1;
        slotsForGroupsWithMaxSize = persons.Length % groups.Count;
    }

    while (relationList.Count > 0)
    {
        decimal[] relationPoints = new decimal[groups.Count];
        for (int i = 0; i < relationPoints.Length; i++)
        {
            relationPoints[i] = 0;
        }

        int[] groupNumberOfPersons = new int[] { -1, -1 };

        //find 
        for (int i = 0; i < groups.Count; i++)
        {
            if (groups[i].Members.Contains(persons.SingleOrDefault(item => item.Id == relationList[0].Item1)))
            //if person 1 is in this group
            {
                groupNumberOfPersons[0] = i;
            }
            if (groups[i].Members.Contains(persons.SingleOrDefault(item => item.Id == relationList[0].Item2)))
            //if person 2 is in this group
            {
                groupNumberOfPersons[1] = i;
            }
        }

        //if both not added
        if (groupNumberOfPersons[0] == -1 && groupNumberOfPersons[1] == -1)
        {
            for (int i = 0; i < groups.Count; i++)
            {
                if ((groups[i].Members.Length < groupMaxSize - 1 && slotsForGroupsWithMaxSize > 0) || groups[i].Members.Length < groupMaxSize - 2)
                {
                    for (int j = 0; j < groups[i].Members.Length; j++)
                    {
                        //sum of all the times the next two people has worked 
                        //with the other members in current group
                        relationPoints[i] += relations.TimesWorkedTogether(
                            groups[i].Members[j].Id, relationList[0].Item1);
                        relationPoints[i] += relations.TimesWorkedTogether(
                            groups[i].Members[j].Id, relationList[0].Item2);
                    }
                    //relationPoints per member in group
                    if (groups[i].Members.Length > 0)
                    {
                        relationPoints[i] = relationPoints[i] / (decimal)groups[i].Members.Length;
                    }
                }
                else
                {
                    relationPoints[i] = int.MaxValue;
                }
            }
            int groupNumber = relationPoints.ToList().IndexOf(relationPoints.Min());
            AddFromListToGroup3(groupNumber, persons, relations, relationList);

            //remove one slot from groups able to have max number of members if max members in group
            if (groups[groupNumber].NumberOfMembers == groupMaxSize)
            {
                slotsForGroupsWithMaxSize--;
            }
        }
        //if one added
        else if (groupNumberOfPersons[0] == -1 || groupNumberOfPersons[1] == -1)
        {
            int groupToAddTo;
            if (groupNumberOfPersons[0] == -1)
            {
                groupToAddTo = groupNumberOfPersons[1];
            }
            else
            {
                groupToAddTo = groupNumberOfPersons[0];
            }
            if ((groups[groupToAddTo].Members.Length < groupMaxSize && slotsForGroupsWithMaxSize > 0)
                || groups[groupToAddTo].Members.Length < groupMaxSize - 1) //if group has space for one
            {
                AddFromListToGroup3(groupToAddTo, persons, relations, relationList);

                //remove one slot from groups able to have max number of members if max members in group
                if (groups[groupToAddTo].NumberOfMembers == groupMaxSize)
                {
                    slotsForGroupsWithMaxSize--;
                }
            }
            else //if no space left in group, find most suitible group
            {
                for (int i = 0; i < groups.Count; i++)
                {
                    //if group has space left
                    if ((groups[i].Members.Length < groupMaxSize && slotsForGroupsWithMaxSize > 0)
                        || groups[i].Members.Length < groupMaxSize - 1)
                    {
                        for (int j = 0; j < groups[i].Members.Length; j++)
                        {
                            //sum of all the times the person has worked 
                            //with the other members in current group
                            if (groupNumberOfPersons[0] == -1)  //if the first person is to be added
                            {
                                relationPoints[i] += relations.TimesWorkedTogether(
                                groups[i].Members[j].Id, relationList[0].Item1);
                            }
                            else //if the second person is to be added
                            {
                                relationPoints[i] += relations.TimesWorkedTogether(
                                groups[i].Members[j].Id, relationList[0].Item2);
                            }
                        }
                        //relationPoints per member in group
                        if (groups[i].Members.Length > 0)
                        {
                            relationPoints[i] = relationPoints[i] / (decimal)groups[i].Members.Length;
                        }
                    }
                    else
                    {
                        relationPoints[i] = int.MaxValue;
                    }
                }
                int groupNumber = relationPoints.ToList().IndexOf(relationPoints.Min());
                if (groupNumberOfPersons[0] == -1)  //if the first person is to be added
                {
                    groups[groupNumber].AddMember(persons.SingleOrDefault(item => item.Id == relationList[0].Item1));
                }
                else //if the second person is to be added
                {
                    groups[groupNumber].AddMember(persons.SingleOrDefault(item => item.Id == relationList[0].Item2));

                }

                //remove one slot from groups able to have max number of members if max members in group
                if (groups[groupNumber].NumberOfMembers == groupMaxSize)
                {
                    slotsForGroupsWithMaxSize--;
                }

                relationList.RemoveAt(0);
            }
        }
        //if both added and different groups  // don't need to check this
        else //if ((groupNumberOfPersons[0] != -1 && groupNumberOfPersons[1] != -1) && (groupNumberOfPersons[0] != groupNumberOfPersons[1]))
        {
            relationList.RemoveAt(0);
        }
    }
}

private void AddFromListToGroup3(int groupNumber, Person[] persons, RelationCalc relations,
                        List<Tuple<int, int, int>> relationList)
{
    //add persons if they are not part of group
    if (!groups[groupNumber].Members.Contains(persons.SingleOrDefault(item => item.Id == relationList[0].Item1)))
    {
        groups[groupNumber].AddMember(persons.SingleOrDefault(item => item.Id == relationList[0].Item1));
    }
    if (!groups[groupNumber].Members.Contains(persons.SingleOrDefault(item => item.Id == relationList[0].Item2)))
    {
        groups[groupNumber].AddMember(persons.SingleOrDefault(item => item.Id == relationList[0].Item2));
    }

    //remove added relation group
    relationList.RemoveAt(0);

}

private void PopulateGroups2(Person[] persons, RelationCalc relations)
{
    List<Tuple<int, int>> maxWorkedWithList = relations.OrderedTimesWorkedList;

    while (maxWorkedWithList.Count > 0)
    {
        decimal[] relationPoints = new decimal[groups.Count];
        for (int i = 0; i < relationPoints.Length; i++)
        {
            relationPoints[i] = 0;
        }
        for (int i = 0; i < groups.Count; i++)
        {
            if (groups[i].Members.Length < (persons.Length / groups.Count)) //if group is not filled
            {
                for (int j = 0; j < groups[i].Members.Length; j++)
                {
                    relationPoints[i] += relations.TimesWorkedTogether(maxWorkedWithList[0].Item1, j);
                }
                //relationPoints per member in group
                if (groups[i].Members.Length > 0)
                {
                    relationPoints[i] = relationPoints[i] / (decimal)groups[i].Members.Length;
                }
            }
            else //assign max value to ignore group
            {
                relationPoints[i] = int.MaxValue;
            }
        }
        int groupNumber = relationPoints.ToList().IndexOf(relationPoints.Min());

        //add member to "best" group
        groups[groupNumber].AddMember(persons.SingleOrDefault(item => item.Id == maxWorkedWithList[0].Item1));
        //remove the person that was just added to groups
        maxWorkedWithList.RemoveAt(0);
    }
}

private void PopulateGroups(Person[] persons, RelationCalc relations)
{
    List<Tuple<int, int, int>> relationList = relations.OrderedRelationsList;

    //fill groups with two each.
    for (int i = 0; i < groups.Count; i++)
    {
        AddFromListToGroup(i, persons, relations, relationList);

    }
    while (relationList.Count > 0)
    {
        decimal[] relationPoints = new decimal[groups.Count];
        for (int i = 0; i < relationPoints.Length; i++)
        {
            relationPoints[i] = 0;
        }
        for (int i = 0; i < groups.Count; i++)
        {
            if (groups[i].Members.Length <= (persons.Length / groups.Count) - 1)
            {
                for (int j = 0; j < groups[i].Members.Length; j++)
                {
                    //sum of all the times the next two people has worked 
                    //with the other members in current group
                    relationPoints[i] += relations.TimesWorkedTogether(
                        groups[i].Members[j].Id, relationList[0].Item1);
                    relationPoints[i] += relations.TimesWorkedTogether(
                        groups[i].Members[j].Id, relationList[0].Item2);
                }
                //relationPoints per member in group
                if (groups[i].Members.Length > 0)
                {
                    relationPoints[i] = relationPoints[i] / (decimal)groups[i].Members.Length;
                }
            }
            else
            {
                relationPoints[i] = int.MaxValue;
            }
        }
        int groupNumber = relationPoints.ToList().IndexOf(relationPoints.Min());
        AddFromListToGroup(groupNumber, persons, relations, relationList);
    }
}



private void AddFromListToGroup(int groupNumber, Person[] persons, RelationCalc relations,
                                List<Tuple<int, int, int>> relationList)
{
    //add the two people in the relation that are next on the list
    groups[groupNumber].AddMember(persons.SingleOrDefault(item => item.Id == relationList[0].Item1));
    groups[groupNumber].AddMember(persons.SingleOrDefault(item => item.Id == relationList[0].Item2));
    //save the id of the people added
    int addedId1 = relationList[0].Item1;
    int addedId2 = relationList[0].Item2;
    //remove relations containing the two people that was just added to groups
    relationList.RemoveAll(x => x.Item1 == addedId1
                             || x.Item2 == addedId1
                             || x.Item1 == addedId2
                             || x.Item2 == addedId2);
    //Console.WriteLine("==");
    //foreach (var item in relationList)
    //{
    //    Console.WriteLine(item.Item1 + " " + item.Item2 + " " + item.Item3);
    //}
}

private void AddFromMaxTimesWorkedListToGroup(int groupNumber, Person[] persons,
    RelationCalc relations, List<Tuple<int, int>> maxWorkedList)
{
    //add the two people in the relation that are next on the list
    groups[groupNumber].AddMember(persons.SingleOrDefault(item => item.Id == maxWorkedList[0].Item1));

    //remove the person that was just added to groups
    maxWorkedList.RemoveAt(0);
}

//order all connections by least connections in new list.
//insert first in first group
//remove those people's connections from list
//	//repeat until all groups have two people.
//	add next item to next group
//  remove those people's connections from list

//	//repeat until done
//	check next item and find the not filled group with the least connections to those two new people.
//  remove those people's connections from list

///new idea
///order person list by most times worked with anyone
///insert people in groups where they have the least amount of worked with points