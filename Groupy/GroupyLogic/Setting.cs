﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupyLogic
{
    public class Setting
    {
        public List<Person> Persons { get; set; }
        public List<GroupSet> Groupsets { get; set; }
        public RelationHandler relationHandler { get; set; }
        public int Id { get; set; }
    }
}
