﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupyLogic
{
    public class GroupSet
    {
        static int callCounter = 0;
        private List<Group> groups = new List<Group>();
        public int id;

        public List<Group> Groups
        {
            get
            {
                return groups;
            }
            set
            {
                groups = value;
            }
        }

        public void RandomizeGroupsBySize(int peopleInGroup, int totalPersons)
        {
            // 
        }

        public void RandomizeGroupByNumber(int numberOfGroups, Person[] persons, RelationHandler relations)
        {
            //create new groups
            for (int i = 0; i < numberOfGroups; i++)
            {
                Group newGroup = new Group();
                groups.Add(newGroup);
            }

            //PopulateGroupFetch(persons, relations);
            PopulateGroupFetchBruteforce2(persons, relations);

            RegisterMemberRelations(relations);
        }

        #region current algorithm
        private void PopulateGroupFetchBruteforce2(Person[] persons, RelationHandler relations)
        {
            callCounter++;
            List<Group>[] groupSetAlternatives = new List<Group>[5];
            double[] groupSetAltPointsStdDev = new double[groupSetAlternatives.Length];
            int[][] groupSetAltPoints = new int[groupSetAlternatives.Length][];
            int numberOfAlternatives = 0;
            Random rnd = new Random();

            for (int altNbr = 0; altNbr < groupSetAlternatives.Length; altNbr++)
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("new alt");
                Console.ResetColor();
                //create new groups
                groupSetAlternatives[altNbr] = new List<Group>();
                for (int i = 0; i < groups.Count; i++)
                {
                    groupSetAlternatives[altNbr].Add(new Group());
                }
                List<Tuple<int, int>> peopleList = relations.OrderedTimesWorkedList;

                int groupMaxSize = persons.Length / groupSetAlternatives[altNbr].Count;
                int slotsForGroupsWithMaxSize = groupSetAlternatives[altNbr].Count;
                if (persons.Length % groupSetAlternatives[altNbr].Count > 0)
                {
                    groupMaxSize += 1;
                    slotsForGroupsWithMaxSize = persons.Length % groupSetAlternatives[altNbr].Count;
                }

                int randomPerson = rnd.Next(0, persons.Length - 1);

                //add first person to group one to get started
                groupSetAlternatives[altNbr][0].AddMember(persons.SingleOrDefault(item => item.Id == peopleList[randomPerson].Item1));

                //remove the person that was just added to groups
                peopleList.RemoveAt(randomPerson);

                while (peopleList.Count > 0)
                {
                    for (int i = 0; i < groupSetAlternatives[altNbr].Count; i++)
                    {
                        bool added = false;
                        if ((groupSetAlternatives[altNbr][i].MembersArray.Length < groupMaxSize && slotsForGroupsWithMaxSize > 0)
                           || groupSetAlternatives[altNbr][i].MembersArray.Length < groupMaxSize - 1) //if group has at lest one space left
                        {
                            //this block is to find the person that has worked the leat times with current members
                            int[] currentMembers = new int[groupSetAlternatives[altNbr][i].MembersArray.Length];
                            for (int j = 0; j < currentMembers.Length; j++)
                            {
                                currentMembers[j] = groupSetAlternatives[altNbr][i].MembersArray[j].Id;
                            }
                            int minPoints = int.MaxValue;
                            int indexOfPersonLeastPoints = 0;
                            //Console.WriteLine("zeroing minPoints");
                            for (int j = 0; j < peopleList.Count; j++)
                            {
                                int points = 0;

                                for (int k = 0; k < currentMembers.Length; k++)
                                {
                                    points += relations.TimesWorkedTogether(
                                        peopleList[j].Item1, currentMembers[k]);
                                    //Console.WriteLine($"comparing p {peopleList[j].Item1} and " +
                                    //    $"p {currentMembers[k]}. the points are {relations.TimesWorkedTogether(peopleList[j].Item1, currentMembers[k])}, totpoints: {points}");
                                }
                                if (points < minPoints)
                                {
                                    minPoints = points;
                                    indexOfPersonLeastPoints = j;
                                    //Console.WriteLine($"new lowest for p {peopleList[j].Item1}");
                                }
                            }

                            groupSetAlternatives[altNbr][i].AddMember(persons.SingleOrDefault(
                                    p => p.Id == peopleList[indexOfPersonLeastPoints].Item1));
                            peopleList.RemoveAt(indexOfPersonLeastPoints);

                            //remove one slot from groups able to have max number of members if max members in group
                            if (groupSetAlternatives[altNbr][i].NumberOfMembers == groupMaxSize)
                            {
                                slotsForGroupsWithMaxSize--;
                            }

                            added = true;
                        }

                        if (added)
                            break;
                    }
                }
                //Console.WriteLine(callCounter+ " hello");

                //Console.WriteLine($"optimizing {altNbr}");
                for (int i = 0; true; i++)
                {
                    //if (i % 10 == 0)
                    //{
                    //    Console.WriteLine(i);
                    //}
                        
                    groupSetAlternatives[altNbr] = OptimizeGroupSet(groupSetAlternatives[altNbr], relations, persons, out bool changed);
                    groupSetAltPointsStdDev[altNbr] = Mathematics.StandardDeviation(GroupSetPoints2(groupSetAlternatives[altNbr], relations));
                    if (groupSetAltPointsStdDev[altNbr] == 0 || !changed)
                    {
                        break;
                    }
                    //break;
                }

                groupSetAltPoints[altNbr] = GroupSetPoints2(groupSetAlternatives[altNbr], relations);
                groupSetAltPointsStdDev[altNbr] = Mathematics.StandardDeviation(groupSetAltPoints[altNbr]);

                numberOfAlternatives++;
                //Console.WriteLine("This standard Dev: " + groupSetAltPointsStdDev[altNbr]);
                if (groupSetAltPoints[altNbr].Max() - groupSetAltPoints[altNbr].Min() < 2)
                {
                    break;
                }
            }
            Console.WriteLine("get best alt");
            double bestGroupSet = int.MaxValue;
            for (int i = 0; i < numberOfAlternatives; i++)
            {
                if (groupSetAltPointsStdDev[i] < bestGroupSet)
                {
                    bestGroupSet = groupSetAltPointsStdDev[i];
                    groups = groupSetAlternatives[i];
                    Console.WriteLine($"number {i}, standard Dev: " + groupSetAltPointsStdDev[i]);
                }
            }
            //get the n different alternatives and find the best one and set it to groups
        }

        private static int[] GroupSetPoints(List<Group> groupList, RelationHandler relations)
        {
            int numberOfRelations = 0;
            //calculating the number of relations
            for (int i = 0; i < groupList.Count; i++)//loop the groups
            {
                int n = groupList[i].MembersArray.Count();
                numberOfRelations += n * (n - 1) / 2;
            }

            //Console.WriteLine($"relationscounter = {numberOfRelations}");

            //get the standard deviation of the new number of times worked together for everyone in the groupset
                
            int[] groupSetPoints = new int[numberOfRelations];
            int relationCount = 0;
            for (int i = 0; i < groupList.Count; i++)//loop the groups
            {
                for (int j = 0; j < groupList[i].MembersArray.Count(); j++)//loop the members
                {
                    for (int k = 0; k < j; k++)
                    {
                        if (j != k)
                        {
                            groupSetPoints[relationCount] = relations.TimesWorkedTogether(groupList[i].MembersArray[j].Id, groupList[i].MembersArray[k].Id) + 1;
                            relationCount++;
                        }
                    }
                }
            }
            return groupSetPoints;
        }

        private static int[] GroupSetPoints2(List<Group> groupList, RelationHandler relations)
        {
            List<int> groupSetPoints = new List<int>();
            for (int i = 0; i < relations.RelationMatrix.Length; i++)//loop the y
            {
                int idForI = relations.IndexToId(i);
                for (int j = 0; j < relations.RelationMatrix[i].Length; j++)//loop the x
                {
                    int idForJ = relations.IndexToId(j);

                    bool inSameGroup = false;
                    for (int k = 0; k < groupList.Count; k++)
                    {
                        if (groupList[k].MemberIdArray.Contains(relations.IndexToId(i)) && 
                            groupList[k].MemberIdArray.Contains(relations.IndexToId(j))) //if both are in same group
                        {
                            groupSetPoints.Add(relations.RelationMatrix[i][j].TimesWorked+1);
                            inSameGroup = true;
                        }
                    }
                    if (!inSameGroup)
                    {
                        groupSetPoints.Add(relations.RelationMatrix[i][j].TimesWorked);
                    }
                }
            }
            return groupSetPoints.ToArray();
        }
        #endregion


        public static List<Group> OptimizeGroupSet(List<Group> groupList, RelationHandler relation, Person[] persons, out bool changed)
        {
            var groupSetPoints = GroupSetPoints2(groupList, relation);
            double standardDeviation = Mathematics.StandardDeviation(groupSetPoints);
            int medianWorkedWith = Mathematics.Median(groupSetPoints);
            int leastDiffTimesWorked = groupSetPoints.Max() - groupSetPoints.Min();
            int[] newGroupSetPoints;
            Console.WriteLine("optimizing...");
            Console.WriteLine($"orig sd:{standardDeviation}");

            List<Person> possibleSwitchers = PossibleSwitchers(groupList, medianWorkedWith, relation);
            //List<Person> possibleSwitchers = persons.ToList();

            bool done = false;
            changed = false;
            for (int i = 0; i < possibleSwitchers.Count && !done; i++)
            {
                for (int j = 0; j < persons.Length && !done; j++)
                {
                    if (possibleSwitchers[i] != persons.SingleOrDefault(p => p.Id == j))
                    {
                        groupList = SwitchMembers(groupList, possibleSwitchers[i], persons.SingleOrDefault(p => p.Id == j));
                        //foreach (var item in GroupSetPoints2(groupList, relation))
                        //{
                        //    Console.Write(item+", ");
                        //}
                        //Console.WriteLine();
                        newGroupSetPoints = GroupSetPoints2(groupList, relation);
                        double newStdDev = Mathematics.StandardDeviation(newGroupSetPoints);
                        if (newStdDev < standardDeviation )//&& 
                           //(newGroupSetPoints.Max() - newGroupSetPoints.Min()) <= leastDiffTimesWorked)
                        {
                            standardDeviation = newStdDev;
                            leastDiffTimesWorked = newGroupSetPoints.Max() - newGroupSetPoints.Min();
                            Console.WriteLine($"new internal sd:{newStdDev}, {PrintNumberOfEachInArray(newGroupSetPoints)}");
                            changed = true;
                        }
                        else if (newStdDev == standardDeviation)
                        {

                        }
                        else
                        {
                            groupList = SwitchMembers(groupList, persons.SingleOrDefault(p => p.Id == j), possibleSwitchers[i]);
                        }
                        if (newStdDev==0)
                        {
                            done = true;
                        }
                    }
                }
            }
            return groupList;
        }

        static List<Person> PossibleSwitchers(List<Group> groupList, int medianWorkedWith, RelationHandler relation)
        {
            List<Person> possibleSwitchers = new List<Person>();
            for (int i = 0; i < groupList.Count; i++) //loop through groups
            {
                for (int j = 0; j < groupList[i].MembersArray.Length; j++) //loop through members in group
                {
                    if (relation.MaxTimesWorkedTogether(groupList[i].MembersArray[j].Id, groupList[i].MemberIdArray) + 1 > medianWorkedWith)
                    {
                        possibleSwitchers.Add(groupList[i].MembersArray[j]);
                    }
                }
            }

            //Console.WriteLine("possible switchers:");
            //foreach (var person in possibleSwitchers) //debug print
            //{
            //    Console.WriteLine(person.Id);
            //}
            return possibleSwitchers;
        }

        static List<Group> SwitchMembers(List<Group> groupList,Person p1,Person p2)
        {
            //Console.WriteLine("new switch");
            for (int i = 0; i < groupList.Count; i++) //loop through groups
            {
                //Console.WriteLine($"group {i}");
                if (groupList[i].MembersArray.Contains(p1) && groupList[i].MembersArray.Contains(p2))
                {
                    //nothing
                }
                else if (groupList[i].MembersArray.Contains(p1))
                {
                    //Console.WriteLine($"removing {p1.Id}, adding {p2.Id}");
                    groupList[i].RemoveMember(p1);
                    groupList[i].AddMember(p2);
                }
                else if (groupList[i].MembersArray.Contains(p2))
                {
                    //Console.WriteLine($"removing {p2.Id}, adding {p1.Id}");
                    groupList[i].RemoveMember(p2);
                    groupList[i].AddMember(p1);
                }
            }
            return groupList;
        }

        private void RegisterMemberRelations(RelationHandler relations)
        {
            //register relations between groupmembers
            foreach (var group in groups)
            {
                for (int i = 0; i < group.NumberOfMembers; i++)
                {
                    for (int j = 0; j < i; j++)
                    {
                        relations.NewGroupRelation(group.MembersArray[i].Id, group.MembersArray[j].Id);
                    }
                }
            }
        }


        public string ShowGroups()
        {
            string output = "";
            for (int i = 0; i < groups.Count; i++)
            {
                output += "Group " + i + "    " + groups[i].NumberOfMembers + " members.";
                
                Person[] groupMembers = groups[i].MembersArray;
                foreach (var groupMember in groupMembers)
                {
                    output += "\n"+groupMember.Name + " " + groupMember.Id;
                }
                output += "\n===================\n";
            }
            return output;
        }

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        //for debugging
        static string PrintNumberOfEachInArray(int[] array)
        {
            Dictionary<int, int> NumberOfEach = new Dictionary<int, int>();
            for (int i = 0; i < array.Length; i++)
            {
                if (NumberOfEach.ContainsKey(array[i]))
                {
                    NumberOfEach[array[i]]++;
                }
                else
                {
                    NumberOfEach.Add(array[i], 1);
                }
                
            }
            var orderedNumberOfEach = NumberOfEach.OrderBy(x => x.Value);
            string output = "";
            foreach (var each in orderedNumberOfEach)
            {
                output += $"{each.Value} of {each.Key}, ";
            }
            return output;
        }
    }
}
