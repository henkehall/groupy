﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupyLogic
{
    public class PersonRelation
    {
        public int Id { get; set; }
        int timesWorked;
        bool avoid;
        [Required]
        public RelationHandler RelationHandler { get; set; }

        public PersonRelation()
        {
            timesWorked = 0;
            avoid = false;
        }

        public void AddTimesWorked()
        {
            timesWorked++;
        }

        public int TimesWorked
        {
            get
            {
                return timesWorked;
            }
            set
            {
                timesWorked = value;
            }
        }

        public bool Avoid
        {
            get
            {
                return avoid;
            }
            set
            {
                avoid = value;
            }
        }


    }
}