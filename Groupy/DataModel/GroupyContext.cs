﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GroupyLogic;

namespace DataModel
{
    public class GroupyContext:DbContext
    {
        public DbSet<Person> Persons { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupSet> Groupsets { get; set; }
        public DbSet<PersonRelation> Personrelations { get; set; }
        public DbSet<RelationHandler> RelationCalcs { get; set; }
        public DbSet<Setting> Settings { get; set; }
    }
}
