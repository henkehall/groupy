﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GroupyLogic;

namespace GroupyConsole
{
    public class TestData
    {
        public static Person[] randomPersons(int n)
        {
            Person[] persons = new Person[n];
            Random random = new Random();
            for (int i = 0; i < n; i++)
            {
                string fName = RandomString(6, random);
                fName = fName.First().ToString().ToUpper() + String.Join("", fName.Skip(1));
                string lName = RandomString(8, random);
                lName = fName.First().ToString().ToUpper() + String.Join("", lName.Skip(1));
                persons[i] = new Person(fName + " " + lName);
            }
            return persons;
        }

        public static string RandomString(int length, Random random)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzåäö";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
