﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GroupyLogic;

namespace GroupyConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var rnd = new Random();
            //Person[] persons = TestData.randomPersons(rnd.Next(8, 30));
            Person[] persons = TestData.randomPersons(25);
            //foreach (var person in persons)
            //{
            //    Console.WriteLine(person.Name);
            //}
            Console.WriteLine("\n\n=================");

            RelationHandler calcy = new RelationHandler(persons);

            List<GroupSet> groupSets = new List<GroupSet>();
            groupSets.Add(new GroupSet());
            groupSets[0].RandomizeGroupByNumber(5, persons,calcy);

            Console.WriteLine(groupSets[0].ShowGroups());
            
            calcy.PrintRelationMatrix_TimesWorked();

            int numberOfSets = 4;
            
            for (int i = 1; i < numberOfSets; i++)
            {
                groupSets.Add(new GroupSet());
                //groupSets[i].RandomizeGroupByNumber(rnd.Next(2,6), persons, calcy);
                groupSets[i].RandomizeGroupByNumber(5, persons, calcy);

                Console.WriteLine(groupSets[i].ShowGroups());

                calcy.PrintRelationMatrix_TimesWorked();
            }
        }
    }
}

/////////// ööööhh, kind of ok algorithm, but lacking in the looking ahead department.
/////////// Do something to help that.

    /////// Look at past groupings and have a value for last time worked together, so that the value of one WT decreses over time.

    /////// If times worked with person is over median, redo. <= would this work?

    /////// pick another person and switch and see if standard deviation changes. loop through all alternatives.